
 HARDENING LINUX BOXES RHEL7/8 Centos7/8  Oracle Linux 7/8  Ubuntu18 SLES15
 
 BE CAREFULL IF RUNNING THE PLAYBOOK ON PRODUCTION SERVER!!! THE HARDENING MAY BREAK STUFF!!!
 
 Read the "control_list.md" for list of tags that are available for running specific tasks only.
 
 Download playbook from zerhatic GIT repository:
      
     git clone https://gitlab.com/erhat/ansible-hardening.git

 For CIS hardening use ansible playbook main.yml. Be sure zyou are in directory with the following structure:
  
 .
 |-- ansible.cfg
 |-- collections
 |   `-- requirements.yml
 |-- controls_list.md
 |-- inventory
 |-- main.yml
 `-- README.md 
 
 Before running playbook download the hardening collection with:
  
     ansible-galaxy collection install -r collections/requirements.yml -p collections/  
 
 When inside run:
 
     ansible-playbook -i <IP ADDRESS>, main.yml -u <your_user> -k -K -b
 
 or if you define ip address in "inventory" file:
 
     ansible-playbook main.yml -u <your_user> -k -K -b

You may skip some tasks or run only specified tasks using tags with --skip-tags and --tags option.
 
For instance, let's say we want to skip the task that forces change of password in 365 days. Consulting "control_list.md" we can see the task is tagged with "5.4.1.1". Use comma tyo select multiple tags.

     ansible-playbook main.yml -u <your_user> -k -K -b --skip-tags=5.4.1.1

After deploymnent ssh login with root user  won't work. Use the user you have defined in playbook Default (localadmin). 
