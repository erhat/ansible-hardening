if [ "$LOGNAME" = root ] || [ "`id -u`" -eq 0 ] ; then

    PS1='[\[\033[01;31m\]\u@\h\[\033[00m\]:\w] : '

else
    PS1='[\[\033[01;32m\]\u@\h\[\033[00m\]:\w] : '
fi
